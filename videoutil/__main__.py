import cv2
import os
import numpy as np
from videoutil import arg
import pandas as pd
from timeit import default_timer as timer
import time



class Clock:
    def __init__(self):
        self._start_time = timer()
        self._pause_time = self._start_time
        self._is_paused = False
        self._previous_time = self._start_time
        self._speed = 1

    def time(self):
        if self._is_paused:
            return self._pause_time - self._start_time
        else:
            current_time = timer()
            time_elapsed = current_time - self._previous_time
            new_time = self._previous_time + self._speed * time_elapsed
            self._start_time += current_time - new_time
            self._previous_time = current_time

            return current_time - self._start_time

    def reset(self):
        self._start_time = timer()
        self._pause_time = self._start_time

    def add_time(self, time):
        self._start_time -= time

    def pause(self):
        self._pause_time = timer()
        self._is_paused = True

    def restart(self):
        self._previous_time = timer()
        new_time = self._previous_time - self._pause_time
        self.add_time(-new_time)
        self._is_paused = False
        self._pause_time = self._start_time

    def speed_up(self):
        if self._speed < 10:
            self._speed *= 2
            self.time()

    def slow_down(self):
        if self._speed > 0.03:
            self._speed /= 2
            self.time()

    def get_speed(self):
        return self._speed



def main():
    space = 32
    esc = 27

    record = arg.record
    opto = arg.opto
    sync_opto = arg.sync_opto
    redo = arg.redo
    pause = False
    pause_frame = False

    using_npz = False
    using_csv = False

    clock = Clock()
    is_first_frame = True
    redo_first_flag = True

    try:
        path_video_in = arg.file[0]
    except:
        print('please select the file you want to use')
        return

    path_video_out = path_video_in[:-4] + '_converted.avi'
    path_npz = path_video_in[:-4] + '.npz'
    path_opto_raw = path_video_in[:-4] + '_opto_raw.csv'
    path_opto = path_video_in[:-4] + '_opto.csv'
    path_csv = path_video_in[:-4] + '.csv'

    try:
        data = np.load(path_npz, allow_pickle=True)
        using_npz = True
        print('using npz file')
    except:
        try:
            data = pd.read_csv(path_csv)
            using_csv = True
            print('using csv file')
        except:
            print('no npz or csv file found')
            #return

    texts = []
    circles0 = []
    circles1 = []
    dates = []

    if using_npz:
        timestamps = data['arr_0']
        frames = data['arr_1']

        # check frames
        # frames are stored with +1 by mistake
        frames = [f -1 for f in frames]

        states = data['arr_2']
        list_texts = data['arr_3']
        list_circles0 = data['arr_4']
        list_circles1 = data['arr_5']

        dates = pd.DatetimeIndex(timestamps)
        dates -= dates[0]
        trials = None
        x = None
        y = None
        led = None

        j = 0
        for i in range(len(timestamps)):
            text = ''
            circle0 = 1000
            circle1 = 1000
            if len(frames) > 0:
                if i == frames[j]:
                    text = list_texts[states[j]]
                    circle0 = list_circles0[states[j]] * 2
                    circle1 = list_circles1[states[j]] * 2 + 200
                    if j < len(frames) - 1:
                        j += 1
            texts.append(text)
            circles0.append(circle0)
            circles1.append(circle1)

    elif using_csv: 
        timestamps = data['timestamps']
        frames = data['frames']
        states = data['states']
        trials = data['trial'] + 1

        x = data['x'].astype(float) * 1280 / 640
        y = data['y'].astype(float) * 960 / 480

        x = x.astype(int)
        y = y.astype(int)
        
        led = data['led_on']
        circles0 = None
        circles1 = None

        dates = pd.DatetimeIndex(timestamps)
        dates -= dates[0]
    
        # in states replace nan with ''
        texts = np.where(states != states, '', states)

        grouped = data.groupby('trial')['states'].apply(lambda x: any(x == 'On')).reset_index()
        data = pd.merge(data, grouped, on='trial', suffixes=('', '_grouped'))
        data['light'] = np.where(data['states_grouped'], 'On', 'Off')
        data.drop('states_grouped', axis=1, inplace=True)
        lights = data['light']

    video = cv2.VideoCapture(path_video_in)

    fps = int(video.get(cv2.CAP_PROP_FPS))

    print("fps ", fps)
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fourcc = int(video.get(cv2.CAP_PROP_FOURCC))
    fourcc = "".join([chr((fourcc >> 8 * i) & 0xFF) for i in range(4)])
    fourcc_out = cv2.VideoWriter_fourcc(*fourcc)


    def get_frame_from_deltatime(delta_timing, i):
        new_time = times[i] + delta_timing
        for i, t in enumerate(times):
            if t > new_time:
                return i
        return None

    if record:
        out = cv2.VideoWriter(path_video_out, fourcc_out, fps, (1280, 960))

    total_frames = video.get(cv2.CAP_PROP_FRAME_COUNT)

    times = [d.total_seconds() for d in dates]



    # find ephys data
    directory = os.path.dirname(path_video_in)
    directory_name = os.path.basename(directory)

    ephys = False

    for i in range(1, 33):
        path = os.path.join(directory, directory_name + '_channel' + str(i) + '.npy')
        if os.path.exists(path):
            ephys = True
            break


    if ephys:
        print('')
        delete_ttls = int(input('how many ttls delete from ephys (0,1,2... default 1): ') or '1')
        channel_number = int(input('chanel number (default 9): ') or '9')
        amp = int(input('amp size (default 100%): ') or '100')
        center = int(input('x axis position from 0 to 960 (default = 480): ') or '480')
        print('')

        delete_ttls = max(0, delete_ttls)
        amp = amp / 100

        path = os.path.join(directory, directory_name + '_channel_sync_TTL.npy')
        path2 = os.path.join(directory, directory_name + '_channel' + str(channel_number) + '.npy')

        try:
            channel = np.load(path)
            channel2 = np.load(path2)
        except:
            print('problem reading the channel')
            return

        ttl_frames = [] # frame of the start of each ttl
        ttl_times = [] # time of the start of each ttl
        ttl_ephyspoint = [] # ephys index of the start of each ttl

        for i in range(len(frames)):
            if i > 0:  # first frame can be an error
                if frames[i] - frames[i - 1] > 1:
                    ttl_frames.append(frames[i])
                    ttl_times. append(times[frames[i]])

        for i in range(len(channel)):
            if i > 0 and channel[i] > 0.5 and channel[i - 1] < 0.5:
                ttl_ephyspoint.append(i)

        ttl_ephyspoint = ttl_ephyspoint[delete_ttls:]

        j = 0
        channel_time = []
        for i in range(len(channel)):
            if i < ttl_ephyspoint[j] or j == len(ttl_ephyspoint) - 1:
                factor = 1000
                channel_time.append(ttl_times[j] + (i - ttl_ephyspoint[j]) / factor)
            else:
                factor = ttl_ephyspoint[j + 1] - ttl_ephyspoint[j]
                channel_time.append(ttl_times[j] + (i - ttl_ephyspoint[j]) / factor)
                j += 1

        j = 0
        channel_timestamp = []
        for i in range(len(channel)):
            if channel_time[i] >= times[j]:
                channel_timestamp.append(i)
                j += 1

        missing = len(times) - len(channel_timestamp)
        if missing > 0:
            for i in range(missing):
                channel_timestamp.append(0)


    if opto or sync_opto:
        df = pd.DataFrame(columns=['frame', 'timestamp', 'state', 'led_on', 'lamp_on'])

        # Extract the region when the mouse is supposed to be
        # You can adjust the values as needed
        x_mouse_1, y_mouse_1, x_mouse_2, y_mouse_2 = 400, 100, 1000, 900
        x_lamp_1, y_lamp_1, x_lamp_2, y_lamp_2 = 700, 100, 800, 200

        color_mouse = (0, 0, 255)
        color_lamp = (0, 255, 0)
        color_led = (255, 0, 0)

        led_on = False
        lamp_on = False
        previous_lamp_on = False
        mean_luminance = 0
        previous_mean_luminance = 0

    i = 0

    while video.isOpened():

        if i == total_frames:
            if record or sync_opto:
                break
            else:
                i = 1
                video.set(cv2.CAP_PROP_POS_FRAMES, i)
                clock.reset()
                pause = True

        if is_first_frame:
            is_first_frame = False
            clock.reset()

        if record:
            ret, frame = video.read()
            frame = cv2.resize(frame, (1280, 960))
        else:
            if pause_frame or not pause:
                video.grab()

                if using_csv or using_npz:
                    delay = times[i] - clock.time()
                    print(delay)
                    if redo and lights[i] != 'On':
                        redo_first_flag = True
                        print(trials[i], ' - Off - ', i, ' - ', texts[i])
                        i += 1
                        continue
                    elif redo and redo_first_flag:
                        clock.add_time(delay)
                        redo_first_flag = False
                    elif delay < 0 and not pause_frame and not redo:
                        i += 1
                        continue
                    elif 0 < delay < 1:
                        time.sleep(delay)

                    #print(trials[i], ' - On - ', i, ' - ', texts[i])

                ret, frame = video.retrieve()

                frame = cv2.resize(frame, (1280, 960))

                cv2.putText(frame, 'SPACE = pause / resume',
                            (20, 30), cv2.FONT_HERSHEY_DUPLEX,
                            0.8, (255, 255, 255), 1, cv2.LINE_AA)

                cv2.putText(frame, 'A / S = slow / fast',
                            (20, 60), cv2.FONT_HERSHEY_DUPLEX,
                            0.8, (255, 255, 255), 1, cv2.LINE_AA)

                cv2.putText(frame, 'D / F = frame back / forward',
                            (20, 90), cv2.FONT_HERSHEY_DUPLEX,
                            0.8, (255, 255, 255), 1, cv2.LINE_AA)

                cv2.putText(frame, 'Z / X = 5 seconds back / forward',
                            (20, 120), cv2.FONT_HERSHEY_DUPLEX,
                            0.8, (255, 255, 255), 1, cv2.LINE_AA)

                cv2.putText(frame, 'C / V = 1 minute back / forward',
                            (20, 150), cv2.FONT_HERSHEY_DUPLEX,
                            0.8, (255, 255, 255), 1, cv2.LINE_AA)

                cv2.putText(frame, 'ESC = exit',
                            (20, 180), cv2.FONT_HERSHEY_DUPLEX,
                            0.8, (255, 255, 255), 1, cv2.LINE_AA)

                speed_str = clock.get_speed()

                if speed_str >= 1:
                    speed_str = "{:.0f}".format(speed_str)
                else:
                    speed_str = "{:.2f}".format(speed_str)


                cv2.putText(frame, 'speed = ' + speed_str + ' x',
                            (1010, 150), cv2.FONT_HERSHEY_DUPLEX,
                             1, (255, 255, 255), 1, cv2.LINE_AA)

        if using_csv or using_npz:
            cv2.putText(frame, str(dates[i])[6:-3], (1000, 50), cv2.FONT_HERSHEY_DUPLEX,
                        1, (255, 255, 255), 1, cv2.LINE_AA)

            if texts[i] != '':
                cv2.putText(frame, texts[i], (1030, 230), cv2.FONT_HERSHEY_DUPLEX,
                            1, (255, 255, 255), 1, cv2.LINE_AA)
                if circles0 is not None and circle1 is not None:
                    cv2.circle(frame, (circles0[i], circles1[i]), 12, (255, 255, 255), -1)

            if trials is not None:
                cv2.putText(frame, 'trial: ' + str(trials[i]), (1030, 260), cv2.FONT_HERSHEY_DUPLEX,
                        1, (255, 255, 255), 1, cv2.LINE_AA)
            
        # if x is not None and y is not None:
        #     if led[i]:
        #         cv2.circle(frame, (x[i], y[i]), 12, (100, 255, 100), -1)
        #     else:
        #         cv2.circle(frame, (x[i], y[i]), 12, (255, 100, 100), -1)
        #         cv2.putText(frame, str(y[i]), (1030, 330), cv2.FONT_HERSHEY_DUPLEX,
        #                 1, (255, 255, 255), 1, cv2.LINE_AA)

        if opto or sync_opto:

            gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            frame_mouse = gray_frame[y_mouse_1:y_mouse_2, x_mouse_1:x_mouse_2]
            frame_lamp = gray_frame[y_lamp_1:y_lamp_2, x_lamp_1:x_lamp_2]

            #draw rectangle of the extracted regions
            cv2.rectangle(frame, (x_mouse_1, y_mouse_1), (x_mouse_2, y_mouse_2), color_mouse, 2)
            cv2.rectangle(frame, (x_lamp_1, y_lamp_1), (x_lamp_2, y_lamp_2), color_lamp, 2)

            _, thresh = cv2.threshold(frame_mouse, 40, 255, cv2.THRESH_BINARY_INV) # Adjust threshold value as needed

            # Find contours which will detect the mouse
            contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            for index, contour in enumerate(contours):

                # You can filter out small contours if needed
                if cv2.contourArea(contour) < 1000:
                     continue
                
                # Draw the contour to visualize it
                # Add the origin offset to the contour
                contour2 = contour + (x_mouse_1, y_mouse_1)
                cv2.drawContours(frame, [contour2], -1, (0, 255, 0), 3)

                # Find the centroid or bounding box of the mouse to get its position
                M = cv2.moments(contour2)
                if M['m00'] != 0:
                    cx = int(M['m10'] / M['m00'])
                    cy = int(M['m01'] / M['m00'])
                    # cx, cy is the centroid of the mouse
                else:
                    cx, cy = 0, 0

                # Detecting the LED
                # Get the luminance of the pixels in the contour 
                # and find the maximum luminance
                # Create a mask from the contour
                mask = np.zeros_like(frame_mouse)
                cv2.drawContours(mask, [contour], -1, 255, thickness=cv2.FILLED)

                # Apply the mask to the grayscale image
                masked_image = cv2.bitwise_and(frame_mouse, frame_mouse, mask=mask)

                # Find the maximum luminance value in the masked region
                max_luminance = np.max(masked_image)

                # Find the mean luminance in frame
                mean_luminance = np.mean(frame_lamp)

                cv2.putText(frame, str(max_luminance), (1030, 830), cv2.FONT_HERSHEY_DUPLEX, 
                            1, color_led, 1, cv2.LINE_AA)
                
                cv2.putText(frame, str(mean_luminance), (1030, 930), cv2.FONT_HERSHEY_DUPLEX, 
                            1, color_lamp, 1, cv2.LINE_AA)

                if max_luminance > 150:
                    led_on = True
                else:
                    led_on = False

                if previous_mean_luminance == 0:
                    lamp_on = False
                if mean_luminance > previous_mean_luminance + 5:
                    lamp_on = True
                elif mean_luminance < previous_mean_luminance - 5:
                    lamp_on = False
                else:
                    lamp_on = previous_lamp_on

                previous_lamp_on = lamp_on
                previous_mean_luminance = mean_luminance




                # Draw the centroid of the mouse
                #cv2.circle(frame, (cx, cy +  100), 5, (255, 255, 0), -1)

                # Draw the LED if it is on  
                if led_on:
                   cv2.circle(frame, (cx, cy + 100), 10, color_led, -1)

                # Draw the LAMP if it is on  
                if lamp_on:
                   cv2.circle(frame, (cx, cy + 200), 10, color_lamp, -1)

                if sync_opto:
                    new_row = {'frame': i, 'timestamp': times[i], 'state': texts[i], 'led_on': led_on, 'lamp_on': lamp_on}
                    df = pd.concat([df, pd.DataFrame([new_row])], ignore_index=True)

        if ephys:
            point_x0 = (0, center)
            point_x1 = (1280, center)
            point_y0 = (640, 0)
            point_y1 = (640, 960)

            cv2.line(frame, point_x0, point_x1, (0, 0, 0), 2)
            cv2.line(frame, point_y0, point_y1, (0, 0, 0), 2)


            ms = channel_timestamp[i]

            seconds = int(ms / 1000)
            hours = int(seconds / 3600)
            seconds_left = seconds % 3600
            minutes = int(seconds_left / 60)
            seconds = int(seconds_left % 60)
            ms = int(ms % 1000)

            format_time = "{:02d}:{:02d}:{:02d}.{:03d}".format(hours, minutes, seconds, ms)

            cv2.putText(frame, 'channel = ' + str(channel_number) + ' x ' + str(amp) + ' %',
                        (20, 230), cv2.FONT_HERSHEY_DUPLEX,
                        1, (50, 255, 50), 1, cv2.LINE_AA)


            if channel_timestamp[i] - 640 > 0 and channel_timestamp[i] + 640 < len(channel):

                cv2.putText(frame, format_time, (1015, 100), cv2.FONT_HERSHEY_DUPLEX,
                            1, (50, 255, 50), 1, cv2.LINE_AA)

                for j in range(-640, 640):

                    k = j + 1

                    point1 = (640 + j, center - int(100 * channel[channel_timestamp[i] + j]))
                    point2 = (640 + k, center - int(100 * channel[channel_timestamp[i] + k]))

                    point3 = (640 + j, center - int(amp * channel2[channel_timestamp[i] + j]))
                    point4 = (640 + k, center - int(amp * channel2[channel_timestamp[i] + k]))

                    cv2.line(frame, point1, point2, (225, 255, 225), 2)
                    cv2.line(frame, point3, point4, (50, 255, 50), 2)


            if channel[channel_timestamp[i]] > 0.5:
                if circles0 is not None and circles1 is not None:
                    cv2.circle(frame, (circles0[i], circles1[i] + 30), 12, (50, 255, 50), -1)


        if record:
            out.write(frame)
            i += 1
            percentage = i / total_frames * 100
            print('recording ' + str(percentage) + ' %')

        elif sync_opto:
            i += 1
            percentage = i / total_frames * 100
            print('syncing ' + str(percentage) + ' %')

        else:
            if pause_frame or not pause:
                pause_frame = False
                cv2.imshow('frame', frame)
            if not pause:
                if clock.get_speed() >= 1:
                    i += 1
                else:
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
            k = cv2.waitKey(1) & 0xff
            if k == esc:
                break
            if k == ord('z'):
                if pause:
                    pause_frame = True
                if using_csv or using_npz:
                    j = get_frame_from_deltatime(-5, i)
                    if j is not None:
                        delta_timing = times[j] - times[i]
                        clock.add_time(delta_timing)
                        i = j
                        video.set(cv2.CAP_PROP_POS_FRAMES, i)
                else:
                    i = max(i -300, 1)
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
            if k == ord('x'):
                if pause:
                    pause_frame = True
                if using_csv or using_npz:
                    j = get_frame_from_deltatime(5, i)
                    if j is not None:
                        delta_timing = times[j] - times[i]
                        clock.add_time(delta_timing)
                        i = j
                        video.set(cv2.CAP_PROP_POS_FRAMES, i)
                else:
                    i = i + 300
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
            if k == ord('c'):
                if pause:
                    pause_frame = True
                if using_csv or using_npz:
                    j = get_frame_from_deltatime(-60, i)
                    if j is not None:
                        delta_timing = times[j] - times[i]
                        clock.add_time(delta_timing)
                        i = j
                        video.set(cv2.CAP_PROP_POS_FRAMES, i)
                else:
                    i = max(i -3000, 1)
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
            if k == ord('v'):
                if pause:
                    pause_frame = True
                if using_csv or using_npz:
                    j = get_frame_from_deltatime(60, i)
                    if j is not None:
                        delta_timing = times[j] - times[i]
                        clock.add_time(delta_timing)
                        i = j
                        video.set(cv2.CAP_PROP_POS_FRAMES, i)
                else:
                    i = i + 3000
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
            if k == space and i < total_frames - 1:
                pause = not pause
                if pause:
                    clock.pause()
                else:
                    clock.restart()
            if k == ord('d') and pause and i > 1:
                pause_frame = True
                if using_csv or using_npz:
                    delta_timing = times[i - 1] - times[i]
                    clock.add_time(delta_timing)
                i -= 1
                video.set(cv2.CAP_PROP_POS_FRAMES, i)
            if k == ord('f') and pause and i < total_frames - 1:
                pause_frame = True
                if using_csv or using_npz:
                    delta_timing = times[i + 1] - times[i]
                    clock.add_time(delta_timing)
                i += 1
                video.set(cv2.CAP_PROP_POS_FRAMES, i)
            if k == ord('s'):
                clock.speed_up()
                if pause:
                    pause_frame = True
            if k == ord('a'):
                clock.slow_down()
                if pause:
                    pause_frame = True

    video.release()
    if record:
        out.release()
    elif sync_opto:
        df.to_csv(path_opto_raw, index=False)
    cv2.destroyAllWindows()


    if sync_opto:

        # For every time the state changes to light_on, i will find the closest lamp_on
        # and save the timestamps of both frames in a new df 

        # create a new dataframe to store the data 
        df2 = pd.DataFrame(columns=['from_ttl_to_label_on', 
                                    'from_ttl_to_light_on', 
                                    'from_ttl_to_label_off', 
                                    'from_ttl_to_light_off'])

        df['previous_lamp_on'] = df['lamp_on'].shift(1)
        df['previous_led_on'] = df['led_on'].shift(1)
        df['previous_state'] = df['state'].shift(1)

        df_lamp_on = df.loc[(df['lamp_on'] == True) & (df['previous_lamp_on'] == False)]
        df_lamp_off = df.loc[(df['lamp_on'] == False) & (df['previous_lamp_on'] == True)]

        df_led_on = df.loc[(df['led_on'] == True) & (df['previous_led_on'] == False)]
        df_led_off = df.loc[(df['led_on'] == False) & (df['previous_led_on'] == True)]

        df_on = df.loc[(df['state'] == 'On') & (df['previous_state'] != 'On')]
        df_off = df.loc[(df['state'] == 'Miss') & (df['previous_state'] != 'Miss')]





        first_trial_flag = True

        for index, row in df_lamp_on.iterrows():

            # skip first trial
            if first_trial_flag:
                first_trial_flag = False
                continue

            # get the timestamp 
            timestamp = row['timestamp']

            # find the closest On state
            closest_on_state = df_on.iloc[(df_on['timestamp'] - timestamp).abs().argsort()[:1]]
            closest_on_state = closest_on_state['timestamp'].values[0]

            # find the closest led on
            closest_led_on = df_led_on.iloc[(df_led_on['timestamp'] - timestamp).abs().argsort()[:1]]
            closest_led_on = closest_led_on['timestamp'].values[0]

            # create a new row with the data
            new_row = {'from_ttl_to_label_on': closest_on_state - timestamp, 
                       'from_ttl_to_light_on': closest_led_on - timestamp, 
                       'from_ttl_to_label_off': np.nan,
                       'from_ttl_to_light_off': np.nan}
            
            # add the new row to the new dataframe
            df2 = pd.concat([df2, pd.DataFrame([new_row])], ignore_index=True)






        first_trial_flag = True

        for index, row in df_lamp_off.iterrows():

            # skip first trial
            if first_trial_flag:
                first_trial_flag = False
                continue

            # get the timestamp 
            timestamp = row['timestamp']

            # find the closest Off state
            closest_off_state = df_off.iloc[(df_off['timestamp'] - timestamp).abs().argsort()[:1]]
            closest_off_state = closest_off_state['timestamp'].values[0]

            # find the closest led off
            closest_led_off = df_led_off.iloc[(df_led_off['timestamp'] - timestamp).abs().argsort()[:1]]
            closest_led_off = closest_led_off['timestamp'].values[0]

            # create a new row with the data
            new_row = {'from_ttl_to_label_on': np.nan, 
                       'from_ttl_to_light_on': np.nan, 
                       'from_ttl_to_label_off': closest_off_state - timestamp,
                       'from_ttl_to_light_off': closest_led_off - timestamp}
            
            # add the new row to the new dataframe
            df2 = pd.concat([df2, pd.DataFrame([new_row])], ignore_index=True)


                
        # save the new dataframe to a csv file
        df2.to_csv(path_opto, index=False)


        print("from_ttl_to_label_on: ", df2.from_ttl_to_label_on.mean(), " --- ", df2.from_ttl_to_label_on.std())
        print("from_ttl_to_light_on: ", df2.from_ttl_to_light_on.mean(), " --- ", df2.from_ttl_to_light_on.std())
        print("from_ttl_to_label_off: ", df2.from_ttl_to_label_off.mean(), " --- ", df2.from_ttl_to_label_off.std())
        print("from_ttl_to_light_off: ", df2.from_ttl_to_light_off.mean(), " --- ", df2.from_ttl_to_light_off.std())



        # # read the path csv file
        # session = pd.read_csv(path_csv)
        # # select only the following columns: 
        # # 'trial', 'STATE_Light_on_START', 'STATE_Miss_START'
        # session = session[['trial', 'STATE_Light_on_START', 'STATE_Miss_START']]
        # # rename the columns, to trial, light_on, miss
        # session.columns = ['trial', 'light_on', 'miss'] 
        

if __name__ == '__main__':
    main()
