# videoutil documentation

Package to play videos with annotations in .npy format or to create new videos with the annotations included.

## How to use it

1. Open a terminal window, activate your python environment and go to a suitable location.

2. Download the package:

    `git clone git@bitbucket.org:delaRochaLab/videoutil.git`

3. Go to the package directory.

    `cd videoutil`

4. Install it (remember to have your desired environment activated):

    `pip install -e .`
    
5. To play the video:

    `videotutil 'path_to_video_file'`
    
5. To record a new video with the annotations:

    `videotutil -r 'path_to_video_file'`
    
The annotations file must be located in the same folder than the video and they must have the same name
(video with the extension .avi and annotations with the extension .npz).


## Controls

When playing video:

- A to slow down x2

- S to spped up x2

- Z to go back 5 seconds

- X to go forward 5 seconds

- C to go back 1 minute

- V to go forward 1 minute

- D to go back 1 frame when video is paused

- F to go forward 1 frame when video is paused

- SPACE to pause / play

- ESC to exit
